import { Component, OnInit } from '@angular/core';
import { Http } from '@angular/http'

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.css']
})

export class AppComponent implements OnInit {

  // section: properties with default initializers
  title: string = 'app';
  numbers: number[] = [];

    constructor(private _httpService: Http) { }
    apiValues: string[] = [];
    ngOnInit() {
        this._httpService.get('/api/basic').subscribe(values => {
            this.numbers = values.json() as number[];
        });
    }
}
