﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace asp_net_core_angular.Controllers
{
    [Produces("application/json")]
    [Route("api/Artist")]
    public class ArtistController : Controller
    {
        [HttpGet]
        public IEnumerable<string> Get()
        {
            return new string[] { "James", "Khan" };
        }
    }
}