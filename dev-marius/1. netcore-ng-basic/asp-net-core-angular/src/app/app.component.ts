import { Component, OnInit } from '@angular/core';
import { Http } from '@angular/http';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  //title = 'app';
    constructor(private _httpService: Http) {}

    artists: string[] = [];

    ngOnInit() {
        console.log('something');
        this._httpService.get('/api/artist').subscribe(values => {
            this.artists = values.json() as string[];
            console.log(this.artists);
            console.log(this._httpService);
        });
    }
}
