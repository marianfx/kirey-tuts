import { Component, OnInit } from '@angular/core';
import { Http } from '@angular/http';//service

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  // section: properties with default initializers
  title: string = 'app';
  numbers: number[] = [];


  constructor(private _httpService: Http) { }//inject HttpService

  ngOnInit() {
    // ng uses observables. The subscribe event will be invoked when the http  call finished and returns the values.
    // More on observables: https://angular.io/guide/observables-in-angular
    this._httpService.get("/api/basic")
      .subscribe(values => {
        this.numbers = values.json() as number[];
      });
  }
}
