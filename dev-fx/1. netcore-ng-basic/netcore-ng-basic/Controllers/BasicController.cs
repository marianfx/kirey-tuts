﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;

namespace netcore_ng_basic.Controllers
{
    [Produces("application/json")]
    [Route("api/basic")]
    public class BasicController : Controller
    {
        [HttpGet]
        public IEnumerable<int> Get()
        {
            var r = new Random();
            var N = r.Next(100);
            var output = new List<int>();
            for(var i = 0; i < N; i++)
            {
                output.Add(r.Next(100));
            }

            return output;
        }
    }
}